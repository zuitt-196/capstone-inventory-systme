const dotenv = require("dotenv").config();
const express = require('express');
const  mongoose = require('mongoose');
// const bodyParder = require('body-parser');
const cors = require('cors');
const bodyParser = require("body-parser");
const userRoutes = require('./routes/userRoute');
const errorHandler = require("./middleware/errorMidleware");
const cookieParser = require("cookie-parser");

const app = express();


// BUILD IN MIDDLEWARE FUNCTION SECTIONM
app.use(express.json()); // user communication as json format
app.use(cookieParser()) // respones or send cookie storage pieces of data such as authentication
app.use(express.urlencoded({ extended: false })); 
app.use(bodyParser.json()); // analyze the incomming request in client server 


// ROUTES MIDDLEWARE
app.use('/api/user', userRoutes) // USER ROUTE

//ROUTES  HOME PAGE
app.get('/', (req,res) => {
    res.send("home page")
})


// ERROR MIDDLWARE 
app.use(errorHandler)

const PORT = process.env.PORT || 5000;
// CONNECT THE MONGGO DB and START SERVER 
mongoose.connect(process.env.MONGO_URI).then(() =>{
    app.listen(PORT, ()=>{
        console.log(`Server is running  localhost:${PORT} na lods!!!!`)
    })
}).catch((error) =>{
    console.log(error);
})
