const asyncHandler = require('express-async-handler');
const User = require('../models/User'); 
const jwt  = require('jsonwebtoken');
const bcrypt = require('bcryptjs');


// generate token 
const generateToken  = (id) =>{
    return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: '1d'
  })
}


    // REGISTER USER 
const registerUser = asyncHandler (async (req, res ,next) => {
    // DESTRUCTURET THE REQUEST DATA 
    const {name, email ,password} = req.body;

    // VALDATION IF USER NOT INPUT  NAME AND ECT 
    if (!name || !email || !password) {
            res.status(400);
            throw new Error('Pleas fill in all required fields');
    };

    if (password.length < 6) {
        res.status(400);
        throw new Error('Password must be at least 6 characters');
    }
    //  check if the email exists
    const useremailExists = await User.findOne({email});
    if (useremailExists) {
        res.status(400)
        throw new Error('User email is areaady exists or has been registered');
    }

 
    // CREATE NEW USER 
    const user = await User.create({
        name, 
        email,
        password
    })

          // GENERATE TOKEN
           const token = generateToken(user._id);

         // SEND HTTP-ONLY REQUEST
         res.cookie("token", token, {
            path: '/',
            httpOnly: true,
            exipire: new Date(Date.now() + 1000 * 864000),  // 1 day expiration
            sameSite: "none",
            secure: true

         })

    if (user) {
        // DISTRUCTURE USER DATA 
            const {_id, name , email, photo, phone, bio} = user;
            res.status(201).json({
                _id, 
                name,  
                email,  
                bio, 
                photo,
                phone, 
                token
            })
    }else{
        res.status(400);
        throw new Error('Invalid User data');
    }

});




// USER LOGIN 

const  userLogin = asyncHandler (async (req, res, next) => {

    const {email, password} = req.body;

    // VALIDATE THE REQUEST
    if (!email ||!password) {
        res.status(400);
        throw new Error('Pleas add email and password');
    }


    // check if the user is exist
    const user =  await User.findOne({email});
    if (!user) {
        res.status(400);
        throw new Error('User not found please sing up');
    }


    // user is exist, check if the password is correct
    const passwordIsCorrect =  await bcrypt.compare(password, user.password);

         // GENERATE TOKEN
         const token = generateToken(user._id);

         // SEND HTTP-ONLY REQUEST
         res.cookie("token", token, {
            path: '/',
            httpOnly: true,
            exipire: new Date(Date.now() + 1000 * 864000),  // 1 day expiration
            sameSite: "none",
            secure: true

         })

    if(user && passwordIsCorrect){  
        const {_id, name , email, photo, phone, bio} = user;
        res.status(201).json({
            _id, 
            name,  
            email,  
            bio, 
            photo,
            phone,
            token
        })
    }else{
        res.status(400);
        throw new Error('Invalid email or password');
    }


})  



module.exports = {
    registerUser,
    userLogin
    
  };
  