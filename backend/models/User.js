const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

// define a schema
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please input your name'],
    },
    email: {
        type: String,
        required: [true, 'Please input your email'],
        unique: true,
        trim: true,
        match: [
            /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
            'Please enter a valid email'
        ],
    },
    
    password: {
        type: String,
        required: [true, 'please input password'],
        minLength: [6, "password must be at least 6 characters"],
        // maxLength: [10, "password must not exceed 23 characters"]
    },
    photo: {
        type: String,
        required: [true, 'please input a photo'],
        default: "https://scontent.fceb6-1.fna.fbcdn.net/v/t39.30808-6/387797359_122146943036003702_4149584970964816017_n.jpg?_nc_cat=101&ccb=1-7&_nc_sid=efb6e6&_nc_eui2=AeHXysutWSQoRLNBbM9eR84iTUlOEjsrbslNSU4SOytuyWUXEg_abHN_iGlzol94CxMxPT9CcaP4nA9tgIgbHxTt&_nc_ohc=t_wVgXuBRTYAX_qHWpA&_nc_ht=scontent.fceb6-1.fna&oh=00_AfBUCoMAkPLDQ6VWFV3GCx_4vbs-jO8urKe9rj-T5TXpSw&oe=65A32B27"
    },
    phone: {
        type: String,
        default: "+639",
    },
    bio: {
        type: String,
        maxLenght: [250, 'Bio must be less than 250 characters'],
        default: 'Bio'
    }
},
    {
        timestamps:true
    }
);


// Encrypt password beofore savings to  DB
userSchema.pre("save", async function(next){
    if(!this.isModified('password')){
        return next()
    }


 // ENCRYPT PASSWRORD BEFORE SAVING IN DB FOR SECURITY
 const salt = await bcrypt.genSalt(10);
 const hashedPawssword = await  bcrypt.hash(this.password,salt);
 this.password = hashedPawssword ;
 next();

})



const User = mongoose.model("User", userSchema);
module.exports = User;