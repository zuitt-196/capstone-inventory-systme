const express = require('express');
const {
    registerUser,
    userLogin
    
} = require('../controllers/UserController');
const router = express.Router();



router.post('/register', registerUser); // [ROUTE REGISTRATION]
router.post('/login',  userLogin); // [ROUTE LOGIN]

module.exports = router